<!doctype html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>CCE SBM</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  <!-- Favicons -->
  <link href="{{ url('ebusiness/img/favicon.png') }}" rel="icon">
  <link href="{{ url('ebusiness/img/apple-touch-icon.png') }}" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,400i,600,700|Raleway:300,400,400i,500,500i,700,800,900" rel="stylesheet">

  <!-- Bootstrap CSS File -->
  <link href="{{ url('ebusiness/lib/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  <!-- Libraries CSS Files -->
  <link href="{{ url('ebusiness/lib/nivo-slider/css/nivo-slider.css') }}" rel="stylesheet">
  <link href="{{ url('ebusiness/lib/owlcarousel/owl.carousel.css') }}" rel="stylesheet">
  <link href="{{ url('ebusiness/lib/owlcarousel/owl.transitions.css') }}" rel="stylesheet">
  <link href="{{ url('ebusiness/lib/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet">
  <link href="{{ url('ebusiness/lib/animate/animate.min.css') }}" rel="stylesheet">
  <link href="{{ url('ebusiness/lib/venobox/venobox.css') }}" rel="stylesheet">

  <!-- Nivo Slider Theme -->
  <link href="{{ url('ebusiness/css/nivo-slider-theme.css') }}" rel="stylesheet">

  <!-- Main Stylesheet File -->
  <link href="{{ url('ebusiness/css/style.css') }}" rel="stylesheet">

  <!-- Responsive Stylesheet File -->
  <link href="{{ url('ebusiness/css/responsive.css') }}" rel="stylesheet">

  @yield('css')

  <!-- =======================================================
    Theme Name: eBusiness
    Theme URL: https://bootstrapmade.com/ebusiness-bootstrap-corporate-template/
    Author: BootstrapMade.com
    License: https://bootstrapmade.com/license/
  ======================================================= -->
</head>

<body data-spy="scroll" data-target="#navbar-example">

  <div id="preloader"></div>

  <header>
    @include('partials.header-home')
  </header>
  <!-- header end -->

  <!-- Start Slider Area -->
  <div id="home" class="slider-area">
    <div class="bend niceties preview-2">
      @include('home.partials.slider')
    </div>
  </div>
  <!-- End Slider Area -->

  @yield('content')

  <!-- Start Footer bottom Area -->
  <footer>
    @include('partials.footer-home')
  </footer>

  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

  <!-- JavaScript Libraries -->
  <script src="{{ url('ebusiness/lib/jquery/jquery.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/bootstrap/js/bootstrap.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/owlcarousel/owl.carousel.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/venobox/venobox.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/knob/jquery.knob.js') }}"></script>
  <script src="{{ url('ebusiness/lib/wow/wow.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/parallax/parallax.js') }}"></script>
  <script src="{{ url('ebusiness/lib/easing/easing.min.js') }}"></script>
  <script src="{{ url('ebusiness/lib/nivo-slider/js/jquery.nivo.slider.js') }}" type="text/javascript"></script>
  <script src="{{ url('ebusiness/lib/appear/jquery.appear.js') }}"></script>
  <script src="{{ url('ebusiness/lib/isotope/isotope.pkgd.min.js') }}"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyD8HeI8o-c1NppZA-92oYlXakhDPYR7XMY"></script>

  <!-- Contact Form JavaScript File -->
  <script src="{{ url('ebusiness/contactform/contactform.js') }}"></script>

  <script src="{{ url('ebusiness/js/main.js') }}"></script>

  @yield('scripts')
</body>

</html>
